package com.tktzlabs.dishies.Activities;

import com.tktzlabs.dishies.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultsAdapter extends BaseAdapter 
{
	String[] resultNames;
	String[] resultTels;
	String[] resultLat;
	String[] resultLon;
	Context context;
//	int[] imageId;
	private static LayoutInflater inflater = null;

	public ResultsAdapter(ResultActivity resultActivity, String[] prgmNameList, String[] prgmTelList) 
	{
		resultNames = prgmNameList;
		resultTels = prgmTelList;
		context = resultActivity;
//		imageId = prgmImages;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() 
	{
		return resultNames.length;
	}

	public Object getItem(int position) 
	{
		return position;
	}

	public long getItemId(int position) 
	{
		return position;
	}

	public class Holder 
	{
		TextView tv;
		ImageView img;
	}

	@SuppressLint({ "ViewHolder", "InflateParams" })
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		Holder holder = new Holder();
		View rowView;
		rowView = inflater.inflate(R.layout.result_listview, null);
		holder.tv = (TextView) rowView.findViewById(R.id.textView1);
		//holder.img = (ImageView) rowView.findViewById(R.id.imageView1);
		holder.tv.setText(resultNames[position]);
		//holder.img.setImageResource(imageId[position]);
		rowView.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) 
			{
				//Toast.makeText(context, "You Clicked " + result[position], Toast.LENGTH_LONG).show();
                ActionDialog dialog = new ActionDialog(context, resultNames[position], resultTels[position]);
                dialog.show();
			}
		});
				
		return rowView;
	}
}
