package com.tktzlabs.dishies.Activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.factual.driver.Circle;
import com.factual.driver.Factual;
import com.factual.driver.Query;
import com.factual.driver.ReadResponse;
import com.google.common.collect.Lists;
import com.tktzlabs.dishies.R;
import com.tktzlabs.dishies.Providers.GPSProvider;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

// TODO krasne pridavani polozek do listu> https://github.com/nhaarman/ListViewAnimations // TOHLE NAIMPLEMENTIT
// to je dobre :
public class ResultActivity extends Activity
{
	// LogCat tag
	private static final String TAG = ResultActivity.class.getSimpleName();

	protected Factual factual = new Factual("2NTUunEmCyaK9DL9ukV0IKbRYWnhL3lZ2UNAmvs4",
			"fAG1h7PtpyLGIgEyrpoU5mZRkDkVyvVShaFWhdIl");

	private Button btnGoBack;

	// Flag if activity was loaded before or not
	private boolean isFirstLoaded = true;
	
	private double Xlatitude;
	private double Xlongitude;
	private int meters = 500; // later to get from settings
	private String keyword;

	private ListView resultLV;
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// Hiding the action bar
		getActionBar().hide();
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onStart()
	{
		super.onStart();

		FactualRetrievalTask task = new FactualRetrievalTask();
		GPSProvider gpsProvider = new GPSProvider(this);
		// Get longitude and latitude and keyword
		Bundle extras = getIntent().getExtras();

		if (extras != null)
		{
			Xlatitude = extras.getDouble("latitude");
			Xlongitude = extras.getDouble("longitude");
			keyword = extras.getString("keyword");
		}
		else
		{
			finish();
		}

		if (gpsProvider.canGetLocation())
		{
			if (isFirstLoaded) // Load query only once
			{
				setContentView(R.layout.activity_result_loading);
				// Here we have everything we want, so we can construct a query
				Query query;
				if (keyword != null && !keyword.isEmpty())
				{
					query = new Query().within(new Circle(Xlatitude, Xlongitude, meters)).field("cuisine")
							.equal(keyword).sortAsc("$distance").only("name", "address", "tel");
				}
				else
				{
					query = new Query().within(new Circle(Xlatitude, Xlongitude, meters)).sortAsc("$distance")
							.only("name", "address", "tel");
				}
				task.execute(query);
				isFirstLoaded = false;
			}
		}
		else
		{
			setFailScreen("Cant get location");
		}
	}

	protected class FactualRetrievalTask extends AsyncTask<Query, Integer, List<ReadResponse>>
	{
		@Override
		protected List<ReadResponse> doInBackground(Query... params)
		{
			List<ReadResponse> results = Lists.newArrayList();

			try
			{
				for (Query q : params)
				{
					results.add(factual.fetch("restaurants-us", q)); // restaurant US put all
				}
			}
			catch (final Exception e)
			{
				Log.e(TAG, e.toString());
				Log.e(TAG, e.fillInStackTrace().toString());

				// update UI
				runOnUiThread(new Runnable()
				{
					public void run()
					{
						setFailScreen("Exception occured " + e.toString());
					}
				});
			}

			return results;
		}

		@Override
		protected void onProgressUpdate(Integer... progress)
		{
		}

		@Override
		protected void onPostExecute(List<ReadResponse> responses)
		{
			List<String> nameList = new ArrayList<String>();
			List<String> telList = new ArrayList<String>();

			int counter = 0;
			for (ReadResponse response : responses)
			{
				for (Map<String, Object> restaurant : response.getData())
				{
					String name = (String) restaurant.get("name");
					@SuppressWarnings("unused")
					String address = (String) restaurant.get("address");
					String phone = (String) restaurant.get("tel");
					@SuppressWarnings("unused")
					Number distance = (Number) restaurant.get("$distance");
					//
					nameList.add(name);
					telList.add(phone);
					//
					counter++;
				}
			}

			// add some stuff
			String[] nameArray = nameList.toArray(new String[nameList.size()]);
			String[] telArray = telList.toArray(new String[telList.size()]);

			// If we have a results the result screen will be shown otherwise fail
			if (counter > 0)
			{
				setResultScreen(nameArray, telArray);
			}
			else
			{
				setFailScreen("No results retrieved ... ");
			}
		}
	}

	public void setResultScreen(String[] nameArray, String[] telArray)
	{
		setContentView(R.layout.activity_result);
		resultLV = (ListView) findViewById(R.id.listView);

		// Filling listview with data
		resultLV.setAdapter(new ResultsAdapter(ResultActivity.this, nameArray, telArray));

		// Go back button
		btnGoBack = (Button) findViewById(R.id.btnGoBack);
		btnGoBack.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				finish();
			}
		});
	}

	public void setFailScreen(String reason)
	{
		setContentView(R.layout.activity_result_fail);
		Toast.makeText(getApplicationContext(), "Something went wrong ... (" + reason + ")", Toast.LENGTH_LONG).show();
		
		// Go back button
		btnGoBack = (Button) findViewById(R.id.btnGoBack);
		btnGoBack.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				finish();
			}
		});
	}
}