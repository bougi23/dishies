package com.tktzlabs.dishies.Activities;

import com.tktzlabs.dishies.R;
import com.tktzlabs.dishies.Providers.GPSProvider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DishiesActivity extends Activity 
{
	// LogCat tag
	private static final String TAG = DishiesActivity.class.getSimpleName();

	private Button btnContinue;
	private Button btnTryAgain;
	private EditText txtKeyword;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		/*
		 * Procedura bude nasledujici
		 * 1. Uprostred screeny bude loader s napisem Getting you position, kdyz nedostane pozici, vyhodi to co tinder, 
		 * tj. zapni si GPS nebo neco. [DONE]
		 * 2. Pokud mame pozici, muzeme prejit k zadavani toho co hledami (jidlo steak)
		 * 3. Kliknutim na tlacitko hledame a to tak ze:
		 * 		A> Pokud je resultu vice nez 1, prechazime do dalsi aktivity kde primo zobrazujem v list adapteru
		 * 		B> Pokud je nula resultu, zustavame v DishiesActivite a vypisujem hlasku ze nebylo nic objeveno [DONE]
		 * 
		 * OK a ted to cele zmenim na rezervacni system co posle emaileme rezervaci ! 
		 * V paticce emailu bude reklama, pro zviditelneni nasi appky
		 * mozna potvrzeni rezervace
		 * anebo jako na bookovani vseho (bookies muze byt app name)
		 */
		
		super.onCreate(savedInstanceState);
		// Hiding the action bar
		// getActionBar().hide();
		//
		getActionBar().setDisplayShowHomeEnabled(false);
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) 
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_dishies_actions, menu);
 
        return super.onCreateOptionsMenu(menu);
    }
    
    /**
     * On selecting action bar icons
     * */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {
        // Take appropriate action for each action item click
        switch (item.getItemId())
        {
        case R.id.action_settings:
    		Toast.makeText(getApplicationContext(), "Settings to be done ...", Toast.LENGTH_LONG).show();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
	
	@Override
	protected void onStart() 
	{
			super.onStart();

			GPSProvider gpsProvider = new GPSProvider(this);
			
			if(gpsProvider.canGetLocation()) // If mame pozici, tj mame latitude a longitude
			{
				setMainScreen();
			}
			else // nemame pozici, zobrazujem loader s popisem ze hledame pozici a vyhazujem popup co tinder  s zadosti a zapnuti gps
			{
				setNoGpsScreen();
				gpsProvider.showSettingsAlert();
			}
	}
	
	public void setMainScreen()
	{
		setContentView(R.layout.activity_dishies);
		
		btnContinue = (Button) findViewById(R.id.btnSearch);
		txtKeyword = (EditText) findViewById(R.id.keyword);
		//
		final GPSProvider gpsProvider = new GPSProvider(this);
		//
		final Intent intentToResultActivity = new Intent(this, ResultActivity.class);
		// Continue button
		btnContinue.setOnClickListener(new View.OnClickListener() 
		{
			public void onClick(View v) 
			{
				if(gpsProvider.canGetLocation())
				{
					intentToResultActivity.putExtra("latitude", gpsProvider.getLatitudeAsDouble());
					intentToResultActivity.putExtra("longitude", gpsProvider.getLongitudeAsDouble());
					// Check if we have any keyword
					String keyword = txtKeyword.getText().toString();
					if(keyword != null && !keyword.isEmpty())
					{
						intentToResultActivity.putExtra("keyword", keyword);						
					}
					else
					{
						intentToResultActivity.putExtra("keyword", "");
					}
					//
					startActivity(intentToResultActivity);					
				}
			}
		});
	}
	
	public void setNoGpsScreen()
	{
		setContentView(R.layout.activity_dishies_loader);	
		
		btnTryAgain = (Button) findViewById(R.id.btnTryAgain);	
		
		final GPSProvider gpsProvider = new GPSProvider(this);
		final boolean isPositionAvail = gpsProvider.canGetLocation();
		
		// Try again button
		btnTryAgain.setOnClickListener(new View.OnClickListener() 
		{	
			public void onClick(View v) 
			{				
				Log.d(TAG, "click");
				if(isPositionAvail) // If mame pozici, tj mame latitude a longitude
				{
					setMainScreen();
				}
				else
				{
					gpsProvider.showSettingsAlert();
				}
			}
		});
	}	
}
