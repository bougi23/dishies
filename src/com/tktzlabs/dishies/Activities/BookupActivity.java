package com.tktzlabs.dishies.Activities;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import com.tktzlabs.dishies.R;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class BookupActivity extends Activity
{
    private Button btnBack;
    private Button btnBookup;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bookup);
		// Hiding the action bar
		getActionBar().hide();
		
		// Go back button
		btnBack = (Button) findViewById(R.id.btnBack);
		btnBack.setOnClickListener(new View.OnClickListener() 
		{	
			public void onClick(View v) 
			{				
				finish();
			}
		});

		// Bookup button
		btnBookup = (Button) findViewById(R.id.btnBookup);
		btnBookup.setOnClickListener(new View.OnClickListener() 
		{	
			public void onClick(View v) 
			{				
				Toast.makeText(getBaseContext(), "TBD", Toast.LENGTH_LONG).show();
			}
		});
		
	}
	
	
	
	
	public static void sendData(String name, String to, String from, String subject, String message)
	{
		String content = "";

		try
		{
			/* Sends data through a HTTP POST request */
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost("http://your.website.com");
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("name", name));
			params.add(new BasicNameValuePair("to", to));
			params.add(new BasicNameValuePair("from", from));
			params.add(new BasicNameValuePair("subject", subject));
			params.add(new BasicNameValuePair("message", message));
			httpPost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));

			/* Reads the server response */
			HttpResponse response = httpClient.execute(httpPost);
			InputStream in = response.getEntity().getContent();

			StringBuffer sb = new StringBuffer();
			int chr;
			while ((chr = in.read()) != -1)
			{
				sb.append((char) chr);
			}
			content = sb.toString();
			in.close();

			/* If there is a response, display it */
			if (!content.equals(""))
			{
				Log.i("HTTP Response", content);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	
	// PHP SCRIPT BUDE
	/*
	 * <?php
$name = $_POST['name'];
$to = $_POST['to'];
$from = $_POST['from'];
$subject = $_POST['subject'];
$message = "From: ".$name."\r\n";
$message .= $_POST['message'];
$headers = "From:" . $from;
mail($to,$subject,$message,$headers);
?> 
	 * 
	 * 
	 */
}
