package com.tktzlabs.dishies;

import android.app.Application;
import android.content.Context;

public class Dishies extends Application
{
	// LogCat tag
	//private static final String TAG = Arounder.class.getSimpleName();
	
	private static Context mContext;

	@Override
	public void onCreate() 
	{
		super.onCreate();
		mContext = getApplicationContext();
	}

	public static Context getContext() 
	{
		return mContext;
	}       

}
